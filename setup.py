# Set __version__ in the setup.py
with open('datalogger/version.py') as f: exec(f.read())

from setuptools import setup

setup(name='datalogger',
      description='datalogger is a basic program developed to acquire data from Keysight 34972A device',
      version=__version__,
      packages=['datalogger'],
      scripts=["bin/datalogger-gui"],
      require=['PyQt5', 'pyqtgraph', 'scinstr', 'benutils', 'numpy'],
      url='https://gitlab.com/bendub/datalogger',
      author='Benoit Dubois',
      author_email='benoit.dubois@femto-st.fr',
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: X11 Applications :: Qt',
          'Intended Audience :: End Users/Desktop',
          'License :: OSI Approved :: GNU General Public License (GPL) v3 or later (GPLv3+)',
          'Natural Language :: English',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Scientific/Engineering'],)
