# -*- coding: utf-8 -*-
# datalogger/constants.py

ORGANIZATION = "FEMTO_Engineering"
APP_NAME = 'DataLogger'
APP_BRIEF = "GUI dedicated to handle the DAQ 34972A unit"
AUTHOR_NAME = "Benoit Dubois"
AUTHOR_MAIL = "benoit.dubois@femto-st.fr"
COPYRIGHT = "FEMTO Engineering"
LICENSE = "GNU GPL v3.0 or upper."
