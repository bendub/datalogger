""" datalogger/version.py """
__version__ = "0.3.0b0"

# 0.3.0b0 (01/10/2019): Move to PyQt5 library. Update dependance to libraries
#                       scinstr and benutils. And others "minor" updates.
# 0.2.0a0 (20/01/2015): Move to Python 3. Correct Bug display with EPlotWidget.
