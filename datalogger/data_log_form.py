# -*- coding: utf-8 -*-

"""package widgetben
author    Benoit Dubois
copyright FEMTO ENGINEERING, 2019
license   GPL v3.0+
brief     UI to handle the 34972A data logger application.
"""

from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QSignalMapper
from PyQt5.QtGui import QIcon, QPalette
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QLabel, \
    QAction, QComboBox, QVBoxLayout, QCheckBox, QTabWidget, QDialog, \
    QGroupBox, QLineEdit, QDialogButtonBox, QScrollArea, QPushButton, \
    QFileDialog, QToolButton, QGridLayout, QDoubleSpinBox, QSpinBox

from benutils.widget.real_time_plot import EPlotWidget
from benutils.widget.flowlayout import FlowLayout
from scinstr.daq34972a import FUNCTION, FUNCTION_FULL, INTGT, RESO, \
    TEMP_TRANS_TYPE

from datalogger.constants import APP_NAME


#===============================================================================
class PreferenceDialog(QDialog):
    """PreferenceDialog class, generates the ui of the preference form.
    """

    workspace_changed = pyqtSignal(str)

    def __init__(self, dev, ip="", workspace_dir="", psamp=1.0, \
                 interval_base='day', data_hour=0, parent=None):
        """Constructor.
        :param ip: IP of 34972A device (str)
        :param workspace_dir: workspace where data files are writed (str)
        :param psamp: Acquisition sampling time (float)
        :param interval_base: base period for one file of data in hour (int)
        :param hour_sync: hour of the day we begin the new data file (int)
        :param parent: parent of object (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("Preferences")
        # Lays out
        dev_gbox = self._dev_ui(ip)
        cfg_gbox = self._cfg_ui(workspace_dir, psamp, interval_base, data_hour)
        self._btn_box = QDialogButtonBox(QDialogButtonBox.Ok |
                                         QDialogButtonBox.Cancel)
        main_lay = QVBoxLayout()
        main_lay.addWidget(dev_gbox)
        main_lay.addWidget(cfg_gbox)
        main_lay.addWidget(self._btn_box)
        self.setLayout(main_lay)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)
        # Interface checking logic
        self._check_interface_btn.released.connect(
            lambda: self.check_interface(dev))
        dev.id_checked.connect(self._handle_check_interface_btn)
        # Workspace logic
        self.workspace_changed.connect(self.workspace_lbl.setText)
        self.action_workspace = QAction(QIcon.fromTheme("folder-new"),
                                        "Choose &workspace", self)
        self.action_workspace.setStatusTip("Choose workspace directory")
        self.workspace_btn.setDefaultAction(self.action_workspace)
        self.action_workspace.triggered.connect(self._workspace_dialog)

    def check_interface(self, dev):
        """Check copnnection with device
        :param dev: device instance (object)
        :returns: None
        """
        dev.set_ip(self.ip_led.text())
        dev.check_interface()

    def _dev_ui(self, ip):
        """Build device configuration UI.
        :param idev_ip: IP of input device (str)
        :returns: QGroupBox dedicated to device configuration (object)
        """
        dev_gbox = QGroupBox("Interface")
        self.ip_led = QLineEdit(ip)
        self.ip_led.setInputMask("000.000.000.000;")
        self._check_interface_btn = QPushButton("Check")
        self._check_interface_btn.setToolTip("Check connection with device")
        dev_lay = QVBoxLayout()
        dev_lay.addWidget(QLabel("IP"))
        dev_lay.addWidget(self.ip_led)
        dev_lay.addWidget(self._check_interface_btn)
        dev_gbox.setLayout(dev_lay)
        return dev_gbox

    def _cfg_ui(self, workspace_dir, psamp, interval_base, hour_sync):
        """Build basic configuration UI for the application.
        :param workspace_dir: workspace where data files are writed (str)
        :param psamp: acquisition sampling time (float)
        :param interval_base: base period for one file of data (str)
        :param hour_sync: hour of the day we begin the new data file (int)
        :returns: QGroupBox dedicated to application configuration (object)
        """
        cfg_gbox = QGroupBox("Logger settings")
        self.workspace_btn = QToolButton()
        self.workspace_btn.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.workspace_lbl = QLabel(workspace_dir)
        self.interval_base_cbox = QComboBox()
        interval_base_list = ["Hour", "Day", "Week"]
        if interval_base not in interval_base_list:
            interval_base_index = 1
        else:
            interval_base_index = interval_base_list.index(interval_base)
        self.interval_base_cbox.addItems(interval_base_list)
        self.interval_base_cbox.setCurrentIndex(interval_base_index)
        self.hour_sync_sb = QSpinBox()
        self.hour_sync_sb.setRange(0, 23)
        self.hour_sync_sb.setValue(hour_sync)
        self.psamp_dsb = QDoubleSpinBox()
        self.psamp_dsb.setValue(psamp)
        cfg_layout = QGridLayout()
        cfg_layout.addWidget(QLabel("Workspace directory"), 0, 0, 1, 0)
        cfg_layout.addWidget(self.workspace_btn, 1, 0)
        cfg_layout.addWidget(self.workspace_lbl, 1, 1)
        cfg_layout.addWidget(QLabel("Data file renewal"), 2, 0, 1, 0)
        cfg_layout.addWidget(self.interval_base_cbox, 3, 0)
        cfg_layout.addWidget(QLabel("Period of renewal"), 3, 1)
        cfg_layout.addWidget(self.hour_sync_sb, 4, 0)
        cfg_layout.addWidget(QLabel("Hour of renewal"), 4, 1)
        cfg_layout.addWidget(QLabel("Acquisition sampling time (second)"),
                             5, 0, 1, 0)
        cfg_layout.addWidget(self.psamp_dsb, 6, 0, 1, 0)
        cfg_gbox.setLayout(cfg_layout)
        return cfg_gbox

    @pyqtSlot()
    def _workspace_dialog(self):
        """Choose path to workspace. Call a file dialog box to choose
        the working workspace.
        :returns: choosen workspace else an empty string (str)
        """
        workspace_dir, ok = QFileDialog().getExistingDirectory(
            parent=None,
            caption="Choose workspace directory",
            directory=self.workspace_lbl.text())
        if ok is False:  # If selection canceled
            return ""
        self.workspace_changed.emit(workspace_dir)
        return workspace_dir

    @pyqtSlot(bool)
    def _handle_check_interface_btn(self, is_ok):
        """Handle check_interface_btn look with respect to 'is_ok' argument
        which represent device interface testing response.
        :param is_ok: if True, connection on interface is OK else False (bool)
        :returns: None
        """
        if is_ok is True:
            self._check_interface_btn.setStyleSheet(
                "QPushButton { background-color : green; color : yellow; }")
            self._check_interface_btn.setText("OK")
        else:
            self._check_interface_btn.setStyleSheet(
                "QPushButton { background-color : red; color : blue; }")
            self._check_interface_btn.setText("Fail")

    @property
    def ip(self):
        """Getter of the IP value.
        :returns: IP of device (str)
        """
        return self.ip_led.text()

    @property
    def workspace_directory(self):
        """Getter of the data workspace value.
        :returns: workspace where data files are writed (str)
        """
        return self.workspace_lbl.text()

    @property
    def psamp(self):
        """Getter of the sampling period value in second.
        :returns: sampling period value (float)
        """
        return self.psamp_dsb.value()

    @property
    def interval_base(self):
        """Getter of the interval_base value.
        :returns: interval_base (str)
        """
        return self.interval_base_cbox.currentText()

    @property
    def hour_sync(self):
        """Getter of the hour_sync value.
        :returns: hour_sync value (int)
        """
        return self.hour_sync_sb.value()


#===============================================================================
class ParamWdgt(QWidget):
    """ParamWdgt class, used to handle the graphical configuration of parameters
    of a channel of the 34901 module.
    """

    range_changed = pyqtSignal(int)
    reso_changed = pyqtSignal(int)

    def __init__(self, func_list, parent=None):
        """Constructor: setup ui.
        :param func_list: list of measurement functions supported by channel
        (list of str)
        :retuns: None
        """
        super().__init__(parent=parent)
        self.func_cbox = QComboBox()
        self.range_cbox = QComboBox()
        self.temp_cbox = QComboBox()
        self.res_sb = QSpinBox()
        self.res_sb.setRange(49, 2100)
        self.reso_cbox = QComboBox()
        self.intgt_cbox = QComboBox()
        # Lays out
        layout = QGridLayout()
        layout.addWidget(self.func_cbox, 1, 0, 1, 0)
        layout.addWidget(self.range_cbox, 2, 0, 1, 0)
        layout.addWidget(self.temp_cbox, 3, 0)
        layout.addWidget(self.res_sb, 3, 1)
        layout.addWidget(self.reso_cbox, 4, 0, 1, 0)
        layout.addWidget(self.intgt_cbox, 5, 0, 1, 0)
        layout.setColumnStretch(0, 4)
        layout.setColumnStretch(1, 1)
        self.setLayout(layout)
        # Generates signals
        self.reso_cbox.currentIndexChanged.connect(self.reso_changed.emit)
        self.range_cbox.currentIndexChanged.connect(self.range_changed.emit)
        # UI handling
        self._ui_handling()
        # Inits widgets
        for reso in RESO:
            self.reso_cbox.addItem(reso.caption)
        for intgt in INTGT:
            self.intgt_cbox.addItem(intgt.caption)
        for func in func_list:
            self.func_cbox.addItem(func)

    def _ui_handling(self):
        """Basic ui handling. Handles widget enabling/disabling.
        Resolution and integration time value are linked.
        :returns: None
        """
        self.func_cbox.currentIndexChanged.connect(self.set_range_list)
        self.reso_cbox.currentIndexChanged.connect(
            self.intgt_cbox.setCurrentIndex)
        self.intgt_cbox.currentIndexChanged.connect(
            self.reso_cbox.setCurrentIndex)
        self.range_cbox.currentIndexChanged.connect(self._range_handling)
        self.range_cbox.currentIndexChanged.connect(self.set_temp_list)

    @pyqtSlot(int)
    def _range_handling(self):
        """Specific range parameter handling.
        Resolution value needs to be disabled if range is set to AUTO mode.
        :returns: None
        """
        if self.range_cbox.isEnabled() is True:
            if self.range_cbox.currentIndex() == 0: # Range auto
                self.reso_cbox.setCurrentIndex(0)
                self.reso_cbox.setDisabled(True)
                self.intgt_cbox.setCurrentIndex(0)
                self.intgt_cbox.setDisabled(True)
                self.temp_cbox.setDisabled(True)
                self.res_sb.setDisabled(True)
            elif self.func_cbox.currentIndex() == 6 and \
                (self.range_cbox.currentIndex() == 2 or \
                self.range_cbox.currentIndex() == 3): # RTD
                self.temp_cbox.setEnabled(True)
                self.res_sb.setEnabled(True)
                self.reso_cbox.setEnabled(True)
                self.intgt_cbox.setEnabled(True)
            elif self.func_cbox.currentIndex() == 6 and \
                self.range_cbox.currentIndex() != 2 and \
                self.range_cbox.currentIndex() != 3: # TC or Therm
                self.temp_cbox.setEnabled(True)
                self.res_sb.setDisabled(True)
                self.reso_cbox.setEnabled(True)
                self.intgt_cbox.setEnabled(True)
            else:
                self.reso_cbox.setEnabled(True)
                self.intgt_cbox.setEnabled(True)
        else:
            self.reso_cbox.setDisabled(True)
            self.intgt_cbox.setDisabled(True)
            self.temp_cbox.setDisabled(True)
            self.res_sb.setDisabled(True)

    def reset(self):
        """Resets the widget.
        :returns: None
        """
        self.func_cbox.setCurrentIndex(0)
        self.range_cbox.setCurrentIndex(0)
        self.temp_cbox.setCurrentIndex(0)
        self.res_sb.setValue(100)
        self.reso_cbox.setCurrentIndex(2)

    @property
    def func(self):
        """Getter for function property.
        :returns: index representing the selected function (int)
        """
        return self.func_cbox.currentIndex()

    @property
    def range(self):
        """Getter for range property.
        :returns: index representing the selected range (int)
        """
        return self.range_cbox.currentIndex()

    @property
    def resolution(self):
        """Getter for resolution property.
        :returns: index representing the selected resolution (int)
        """
        return self.reso_cbox.currentIndex()

    @property
    def intgt(self):
        """Getter for integration time property.
        :returns: index representing the selected integration time (int)
        """
        return self.intgt_cbox.currentIndex()

    @property
    def transducer(self):
        """Getter for temperature transducer property.
        :returns: index representing the selected transducer (int)
        """
        return self.temp_cbox.currentIndex()

    @property
    def resistance(self):
        """Getter for resistance property.
        :returns: index representing the selected resistance (int)
        """
        return self.res_sb.value()

    def set_func(self, value):
        """Setter for function property.
        :param value: index representing the selected function (int)
        :returns: None
        """
        self.func_cbox.setCurrentIndex(value)

    def set_range(self, value):
        """Setter for range property.
        :param value: index representing the selected range (int)
        :returns: None
        """
        self.range_cbox.setCurrentIndex(value)

    def set_transducer(self, value):
        """Setter for temperature transducer property.
        :param value: index representing the transducer (int)
        :returns: None
        """
        self.temp_cbox.setCurrentIndex(value)

    def set_resistance(self, value):
        """Setter for transducer resistance property.
        :param value: value of the transducer resistance (int)
        :returns: None
        """
        self.res_sb.setValue(value)

    def set_resolution(self, value):
        """Setter for resolution property.
        :param value: index representing the selected resolution (int)
        :returns: None
        """
        self.reso_cbox.setCurrentIndex(value)

    def set_intgt(self, value):
        """Setter for integration time property.
        :param value: index representing the selected integration time (int)
        :returns: None
        """
        self.intgt_cbox.setCurrentIndex(value)

    @pyqtSlot(int)
    def set_range_list(self, func_id):
        """Sets a new item list in the range combobox in respect with function
        sets in function combobox widget.
        :param func_id: current function identifier (int)
        :returns: None
        """
        self.range_cbox.clear()
        range_list = FUNCTION_FULL[func_id].list
        for item in range_list:
            self.range_cbox.addItem(item.caption)

    @pyqtSlot(int)
    def set_temp_list(self, range_id):
        """Sets a new item list in the temperature combobox in respect with
        transtucer sets in the range combobox widget.
        :param func_id: current function identifier (int)
        :returns: None
        """
        if self.func_cbox.currentIndex() != 6 or \
            self.range_cbox.currentIndex() == 0:
            return
        self.temp_cbox.clear()
        temp_list = TEMP_TRANS_TYPE[range_id].list
        for item in temp_list:
            self.temp_cbox.addItem(item.caption)


#===============================================================================
class ChannelWdgt(QWidget):
    """ChannelWdgt class, used to handle the graphical configuration of a
    channel of the 34901 module.
    """

    state_changed = pyqtSignal(int)
    range_changed = pyqtSignal(int)
    reso_changed = pyqtSignal(int)

    def __init__(self, id_num, func_list, parent=None):
        """Constructor: setup ui.
        :parma id_num: unique identification number (int)
        :param func_list: list of measurement functions supported by channel
        (list of str)
        :retuns: None
        """
        super().__init__(parent=parent)
        self.id = id_num
        self.en_ckbox = QCheckBox("Enabled")
        self.label = QLabel("Channel " + str(id_num))
        self.param_wdgt = ParamWdgt(func_list)
        # Lays out
        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.en_ckbox)
        layout.addWidget(self.param_wdgt)
        self.setLayout(layout)
        # Generates signals
        self.param_wdgt.range_changed.connect(self.range_changed.emit)
        self.param_wdgt.reso_changed.connect(self.reso_changed.emit)
        self.en_ckbox.stateChanged.connect(self.state_changed.emit)
        # UI handling
        self._ui_handling()
        # Inits widget
        self.reset()

    def _ui_handling(self):
        """Basic ui handling. Handles widget enabling/disabling.
        :returns: None
        """
        self.en_ckbox.stateChanged.connect(self.handle_state_change)

    def reset(self):
        """Resets the widget.
        :returns: None
        """
        self.param_wdgt.reset()
        self.setState(Qt.Unchecked)
        self.setEnabled(False)

    @property
    def func(self):
        """Getter for function property.
        :returns: index representing the selected function (int)
        """
        return self.param_wdgt.func

    @property
    def range(self):
        """Getter for range property.
        :returns: index representing the selected range (int)
        """
        return self.param_wdgt.range
    @property
    def transducer(self):
        """Getter for temperature transducer property.
        :returns: index representing the selected transducer (int)
        """
        return self.param_wdgt.transducer

    @property
    def resistance(self):
        """Getter for resistance property.
        :returns: index representing the selected resistance (int)
        """
        return self.param_wdgt.resistance

    @property
    def resolution(self):
        """Getter for resolution property.
        :returns: index representing the selected resolution (int)
        """
        return self.param_wdgt.reso

    @property
    def intgt(self):
        """Getter for integration time property.
        :returns: index representing the selected integration time (int)
        """
        return self.param_wdgt.intgt

    def set_func(self, value):
        """Setter for function property.
        :param value: index representing the selected function (int)
        :returns: None
        """
        self.param_wdgt.set_func(value)

    def set_range(self, value):
        """Setter for range property.
        :param value: index representing the selected range (int)
        :returns: None
        """
        self.param_wdgt.set_range(value)

    def set_transducer(self, value):
        """Setter for temperature transducer property.
        :param value: index representing the transducer (int)
        :returns: None
        """
        self.param_wdgt.set_transducer(value)

    def set_resistance(self, value):
        """Setter for transducer resistance property.
        :param value: value of the transducer resistance (int)
        :returns: None
        """
        self.param_wdgt.set_resistance(value)

    def set_resolution(self, value):
        """Setter for resolution property.
        :param value: index representing the selected resolution (int)
        :returns: None
        """
        self.param_wdgt.set_reso(value)

    def set_intgt(self, value):
        """Setter for integration time property.
        :param value: index representing the selected integration time (int)
        :returns: None
        """
        self.param_wdgt.set_intgt(value)

    def checkState(self):
        """Redefines method: returns the state of widget ie the state of the
        en_ckbox widget instead of the state of the widget itself, because
        the en_ckbox widget master the state of the widget.
        :returns: the state of the check box (int)
        """
        return self.en_ckbox.checkState()

    def setState(self, state):
        """Sets state property. See checkState().
        :param state: the state of the check box (int)
        """
        self.en_ckbox.setCheckState(state)

    def isEnabled(self):
        """Redefines method: returns the state of widget ie the state of the
        en_ckbox widget instead of the state of the widget itself, because
        the en_ckbox widget master the state of the widget.
        :returns: State of widget (bool)
        """
        return self.en_ckbox.isEnabled()

    def setEnabled(self, flag=True):
        """Redefines method: enables/disables whole widgets except en_ckbox
        widget, because the en_ckbox called this function.
        :param flag: new flag (bool)
        :returns: None
        """
        self.param_wdgt.setEnabled(flag)

    def handle_state_change(self, state):
        """Handles widget in respect with QCheckBox state.
        :param state: new state (int)
        :returns: None
        """
        self.setEnabled(True if state == Qt.Checked else False)


#===============================================================================
class Mod34901Wdgt(QWidget):
    """W34901Wdgt class, used to handle the graphical configuration of the
    34901 module.
    """

    channel_state_changed = pyqtSignal(int)
    channel_range_changed = pyqtSignal(int)
    channel_reso_changed = pyqtSignal(int)

    def __init__(self, slot=None, parent=None):
        """Constructor.
        :param slot: slot number, between 1 to 3 (int)
        :returns: None
        """
        super().__init__(parent=parent)
        self._slot = slot
        self._channels = {}
        for id_key in range(100*slot+1, 100*slot+21):
            self._channels[id_key] = ChannelWdgt(id_key, \
                                [idx.caption for idx in FUNCTION])
        for id_key in range(100*slot+21, 100*slot+23):
            self._channels[id_key] = ChannelWdgt(id_key, \
                                [idx.caption for idx in FUNCTION_FULL])
        # Layout
        scroll_layout = QVBoxLayout(self)
        scroll = QScrollArea()
        scroll_layout.addWidget(scroll)
        scroll_wdgt = QWidget(self)
        dyn_layout = FlowLayout(scroll_wdgt)
        for channel in self._channels.values():
            dyn_layout.addWidget(channel)
        scroll.setWidget(scroll_wdgt)
        scroll.setWidgetResizable(True)
        # Channel 21 and 22 are mutualy exclusives
        self._channels[100*slot+21].en_ckbox.stateChanged.connect( \
            lambda: self._exclu_chan(100*slot+21, [100*slot+21, 100*slot+22]))
        self._channels[100*slot+22].en_ckbox.stateChanged.connect( \
            lambda: self._exclu_chan(100*slot+20, [100*slot+21, 100*slot+22]))
        # Generates signals when a channel changes
        ## channel_state_changed()
        state_signal_mapper = QSignalMapper(self)
        for key, channel in self._channels.items():
            state_signal_mapper.setMapping(channel, key)
            channel.state_changed.connect(state_signal_mapper.map)
        state_signal_mapper.mapped.connect(self.channel_state_changed)
        ## channel_range_changed()
        range_signal_mapper = QSignalMapper(self)
        for key, channel in self._channels.items():
            range_signal_mapper.setMapping(channel, key)
            channel.range_changed.connect(range_signal_mapper.map)
        range_signal_mapper.mapped.connect(self.channel_state_changed)
        ## channel_reso_changed()
        reso_signal_mapper = QSignalMapper(self)
        for key, channel in self._channels.items():
            reso_signal_mapper.setMapping(channel, key)
            channel.reso_changed.connect(reso_signal_mapper.map)
        reso_signal_mapper.mapped.connect(self.channel_state_changed)

    def __iter__(self):
        """Iterator: iterates over channels dict.
        """
        return iter(self._channels)

    def itervalues(self):
        """Iterator: iterates over channels dict.
        """
        return iter(self._channels.values())

    def enabled_channels(self):
        """Getter of the list of enabled channel object (list).
        :returns: the list of enabled channel object (list)
        """
        en_list = []
        for channel in self._channels.values():
            if channel.en_ckbox.checkState() == Qt.Checked:
                en_list.append(channel)
        return en_list

    def reset(self):
        """Resets UI.
        :returns: None
        """
        for channel in self._channels.values():
            channel.reset()

    @property
    def slot(self):
        """Getter of the slot number.
        :returns: the dictionnary of channel object (dict)
        """
        return self._slot

    @property
    def channels(self):
        """Getter of the dictionnary of channel object (dict).
        :returns: the dictionnary of channel object (dict)
        """
        return self._channels

    def channel(self, id_key):
        """Getter of channel widget object (ChannelWdgt).
        :param id_key: key index of channel in the dict channel (str)
        :returns: a channel widget object (ChannelWdgt)
        """
        return self._channels[id_key]

    def _exclu_chan(self, channel_id, channel_id_list):
        """Handles mutual exclusif access of channel 20 and 21 (the only
        channels with capabilities of current measurement) ie ensures that
        only one channel is checked (notes that neither of the channels can
        been checked). Checks that 'channel' is the master over others channels.
        If this channel is checked, the others are unchecked.
        :param channel_id: the id number of the current master channel (int)
        :param channel_id_list: list of id of mutual exclusif channels (list)
        :returns: None
        """
        if self._channels[channel_id].en_ckbox.checkState() == Qt.Checked:
            channel_id_list.remove(channel_id)
            for ch in channel_id_list:
                self._channels[ch].en_ckbox.setChecked(Qt.Unchecked)


#===============================================================================
class DataLogWidget(QWidget):
    """DataLogWidget class, main interface of the UI of the data log form.
    """

    CHANNEL_KEYS_LIST = range(101, 123)
    MODULE_SLOT = 1

    def __init__(self, parent=None):
        """Constructor.
        :returns: None
        """
        super().__init__(parent=parent)
        # Lays out
        tab = QTabWidget()
        self.module_widget = Mod34901Wdgt(self.MODULE_SLOT)
        self.plot_widget = self._build_plot_widget(self.CHANNEL_KEYS_LIST)
        scrolla = QScrollArea()
        scrolla.setWidgetResizable(True)
        scrolla.setBackgroundRole(QPalette.Dark)
        scrolla.setWidget(self.plot_widget)
        tab.addTab(self.module_widget, "Configuration")
        tab.addTab(scrolla, "Plot data")
        #tab.addTab(self.dev_widget, "Analyze")
        layout = QVBoxLayout()
        layout.addWidget(tab)
        self.setLayout(layout)
        # Initialization
        for legend in self.plot_widget.dict.legends.values():
            legend.setDisabled(True)
        # Local ui handling
        self.module_widget.channel_state_changed.connect(
            self._channel_state_change)
        self.plot_widget.dict.state_changed.connect(self._update_plot_list)

    def _build_plot_widget(self, channel_keys):
        """Builds widget dedicated to data plotting.
        :param channel_keys: list of key for each of channel in widget (list)
        :returns: data plotting widget (EPlotWidget)
        """
        plot_widget = EPlotWidget(channel_keys, parent=self)
        plot_widget.plot.setTitle("Monitoring")
        return plot_widget

    @staticmethod
    def _build_dev_widget():
        """Builds layout dedicated to deviation analyze.
        :returns: deviation plotting widget (AdevPlotWidget)
        """
        #dev_widget = DevPlotWidget()
        #return dev_widget
        pass

    @pyqtSlot(object, int)
    def _update_plot_list(self, scan, state):
        """Updates plot list, the list of channel to plot.
        :param scan: the scan number of the channel updated (int)
        :param state: the state of the channel updated (Qt.State)
        :returns: None
        """
        if state == Qt.Checked:
            self.plot_widget.hide(scan, False)
        else:
            self.plot_widget.hide(scan, True)

    @pyqtSlot(int)
    def _channel_state_change(self, scan):
        """Updates scan list, the list of channel to scan and update legend
        state of plot widget.
        :param scan: scan number of the channel updated (int)
        :returns: None
        """
        state = self.module_widget.channel(scan).checkState()
        if state == Qt.Checked:
            self.plot_widget.add_curve(scan)
            self.plot_widget.legend(scan).setEnabled(True)
            self.plot_widget.legend(scan).setState(Qt.Checked)
        else:
            self.plot_widget.legend(scan).setDisabled(True)
            self.plot_widget.legend(scan).setState(Qt.Unchecked)
            self.plot_widget.remove_curve(scan)


#===============================================================================
class DataLogMainWindow(QMainWindow):
    """MainWindow class, main interface of the UI of the data log form.
    """

    def __init__(self):
        """Constructor.
        :returns: None
        """
        super().__init__()
        self.setWindowTitle("DataLogger")
        # Lays out
        self._create_actions()
        self._menu_bar = self.menuBar()
        self._populate_menubar()
        self._tool_bar = self.addToolBar("Tool Bar")
        self._populate_toolbar()
        self._tool_bar.setMovable(True)
        self._tool_bar.setFloatable(False)
        self._tool_bar.setAllowedAreas(Qt.AllToolBarAreas)
        self._status_bar = self.statusBar()
        self._data_log_wdgt = DataLogWidget()
        self.setCentralWidget(self._data_log_wdgt)
        # UI handling
        self._ui_handling()
        # Initialization of UI
        self.reset()

    def reset(self):
        """Resets form.
        :returns: None
        """
        self._data_log_wdgt.module_widget.reset()
        self._data_log_wdgt.plot_widget.reset()
        self.action_run.setDisabled(True)
        self.action_save.setDisabled(True)
        self.action_reset.setDisabled(True)

    @property
    def module_widget(self):
        """Getter of module widget (Mod34901Wdgt).
        :returns: a module widget (Mod34901Wdgt)
        """
        return self._data_log_wdgt.module_widget

    @property
    def plot_widget(self):
        """Getter of plot widget (EPlotWidget).
        :returns: a plot widget (EPlotWidget)
        """
        return self._data_log_wdgt.plot_widget

    @property
    def dev_widget(self):
        """Getter of deviation widget (DevPlotWidget).
        :returns: a deviation widget (DevPlotWidget)
        """
        return self._data_log_wdgt.dev_widget

    def set_configuration_mode(self):
        """Set UI in configuration (normal) mode, i.e. preference box is
        accessible.
        returns: None
        """
        self._data_log_wdgt.module_widget.setEnabled(True)
        #self._data_log_wdgt.plot_widget.dict.setEnabled(True)
        self.action_pref.setEnabled(True)

    def set_acquisition_mode(self):
        """Set UI in acquisition mode, i.e. preference box is not accessible.
        returns: None
        """
        self._data_log_wdgt.module_widget.setDisabled(True)
        #self._data_log_wdgt.plot_widget.dict.setDisabled(True)
        self.action_pref.setEnabled(False)

    def _ui_handling(self):
        """Basic (local) ui handling.
        :returns: None
        """
        # Sets run and stop exclusive actions
        self.action_run.triggered.connect(
            lambda: self.action_stop.setEnabled(True))
        self.action_stop.triggered.connect(
            lambda: self.action_run.setEnabled(True))
        self.action_run.triggered.connect(
            lambda: self.action_run.setEnabled(False))
        self.action_stop.triggered.connect(
            lambda: self.action_stop.setEnabled(False))
        self.action_stop.setEnabled(False)
        # Handles UI when the state of a channel changes
        self._data_log_wdgt.module_widget.channel_state_changed.connect(
            self._channel_state_change)

    @pyqtSlot()
    def _channel_state_change(self):
        """Handles ui when the state of channels changes:
        updates the state of the run button of bars.
        :returns: None
        """
        if len(self._data_log_wdgt.module_widget.enabled_channels()) == 0:
            # If no channel selected, do not allow start of acquisition
            self.action_run.setDisabled(True)
        else:
            # If channel(s) selected, allow start of acquisition
            self.action_run.setEnabled(True)

    def _create_actions(self):
        """Creates actions used with bar widgets.
        :returns: None
        """
        self.action_save = QAction(QIcon.fromTheme("document-save"),
                                   "&Save", self)
        self.action_save.setStatusTip("Save data")
        self.action_save.setShortcut('Ctrl+S')
        #
        self.action_save_cfg = QAction("Save &Config", self)
        self.action_save_cfg.setStatusTip("Save the configuration for later")
        self.action_save_cfg.setShortcut('Ctrl+C')
        #
        self.action_load_cfg = QAction("&Load Config", self)
        self.action_load_cfg.setStatusTip("Load a previous configuration")
        self.action_load_cfg.setShortcut('Ctrl+L')
        #
        self.action_quit = QAction(QIcon.fromTheme("application-exit"),
                                   "&Quit", self)
        self.action_quit.setStatusTip("Exit application")
        self.action_quit.setShortcut('Ctrl+Q')
        #
        self.action_reset = QAction(QIcon.fromTheme("edit-clear"),
                                    "R&eset", self)
        self.action_reset.setStatusTip("Reset the configuration")
        self.action_reset.setShortcut('Ctrl+E')
        #
        self.action_pref = QAction(QIcon.fromTheme("preferences-system"),
                                   "&Preferences", self)
        self.action_pref.setStatusTip("Open preference dialog form")
        self.action_pref.setShortcut('Ctrl+P')
        #
        self.action_run = QAction(QIcon.fromTheme("system-run"),
                                  "&Run", self)
        self.action_run.setStatusTip("Start acquisition")
        self.action_run.setShortcut('Ctrl+R')
        #
        self.action_stop = QAction(QIcon.fromTheme("process-stop"),
                                   "S&top", self)
        self.action_stop.setStatusTip("Stop acquisition")
        self.action_stop.setShortcut('Ctrl+T')
        #
        self.action_about = QAction(QIcon.fromTheme("help-about"),
                                    "A&bout", self)
        self.action_about.setStatusTip("About " + APP_NAME)
        self.action_about.setShortcut('Ctrl+B')

    def _populate_menubar(self):
        """Populates the menu bar of the UI
        :returns: None
        """
        self._menu_bar.menu_file = self._menu_bar.addMenu("&File")
        self._menu_bar.menu_edit = self._menu_bar.addMenu("&Edit")
        self._menu_bar.menu_process = self._menu_bar.addMenu("&Process")
        self._menu_bar.menu_help = self._menu_bar.addMenu("&Help")
        self._menu_bar.menu_file.addAction(self.action_save)
        self._menu_bar.menu_file.addSeparator()
        self._menu_bar.menu_file.addAction(self.action_save_cfg)
        self._menu_bar.menu_file.addAction(self.action_load_cfg)
        self._menu_bar.menu_file.addSeparator()
        self._menu_bar.menu_file.addAction(self.action_quit)
        self._menu_bar.menu_edit.addAction(self.action_pref)
        self._menu_bar.menu_process.addAction(self.action_run)
        self._menu_bar.menu_process.addAction(self.action_stop)
        self._menu_bar.menu_process.addAction(self.action_reset)
        self._menu_bar.menu_help.addAction(self.action_about)

    def _populate_toolbar(self):
        """Populates the tool bar of the UI
        :returns: None
        """
        self._tool_bar.addAction(self.action_run)
        self._tool_bar.addAction(self.action_stop)
        self._tool_bar.addAction(self.action_reset)
        self._tool_bar.addAction(self.action_save)


#===============================================================================
def display_ui():
    """Displays the main UI.
    """
    import sys

    def print_slot(arg):
        """Print 'arg' to standard output.
        :param arg: data to display (any)
        :returns: None
        """
        print("ui.print_slot.arg;", arg)

    app = QApplication(sys.argv)
    #ui = ChannelWdgt(101, ("DC Voltage", "AC Voltage", "Resistance"))
    #ui = Mod34901Wdgt(1)
    #ui = DataLogWidget()
    ui = DataLogMainWindow()
    ui.module_widget.channel_state_changed.connect(print_slot)
    ui.show()
    sys.exit(app.exec_())


#===============================================================================
if __name__ == "__main__":
    display_ui()
